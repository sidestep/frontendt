#!/usr/bin/env bb

(require '[clojure.test.check.generators :as gen])
(require '[cognitect.transit :as transit])

(defn gen-int[from to]
  (gen/generate (gen/choose from to)))

(defn gen-str [gen cnt]
  (gen/fmap #(reduce str %) (gen/vector gen cnt)))

(defn gen-var-str [from to]
  (gen-str gen/char-alpha (gen-int from to)))

(def gen-user
  (gen/let [firstName (gen-var-str 0 1000)
            lastName (gen-var-str 0 1000)
            age (gen/choose -1 10000)
            email (gen/elements [(gen/generate (gen-var-str 0 10)), "valid@email.com"])]
    {
     "firstName" firstName
     "lastName" lastName
     "age" age
     "email" email 
     }))

(def writer (transit/writer System/out :json-verbose))
(transit/write writer (gen/generate gen-user))


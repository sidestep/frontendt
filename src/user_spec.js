export const inputs =
    {
        lastName:  {type: 'text', pattern: "^[a-zA-Z]+$", required: true}, 
        firstName: {type: 'text'},
        age: {type: 'number', min: 1, max: 100, step: 1, required: true},
        email: {type: 'email', pattern: "^[a-z]{5,10}@[a-z]{2,5}\\.[a-z]{2,3}$"},
        custodian: {type: 'text', pattern: "^[a-zA-Z]+$"}, 
    };

export const grownup_age = 18;

export const invariants =
    {
        custodian_required: (data) => data.age >= grownup_age || data.custodian != null,
    }



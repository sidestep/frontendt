export function htmlToDom(html)
{
    return document.createRange().
        createContextualFragment(html).
        firstElementChild;
}

export function cmlCaseToWords(str)
{
    return str.replace(/([A-Z])/g, " $1");
}


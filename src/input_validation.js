
const emailRx = "[A-Za-z]{2,20}@[A-Za-z]{2,20}\\.[A-Za-z]{2,20}";

const emptyVals = ['', [], {}, NaN, null, undefined]

const validators = 
    {
        text: function({pattern}, val)
        {
            if(typeof val !== 'string') return undefined; 
            if(pattern == null || new RegExp(pattern).test(val))
                return val;
            return undefined;
        },
        number: function({min, max, step}, val) 
        {
            const num = Number(val);
            if(!val || num === NaN) 
                return undefined;
            if((step == null || Number.isInteger(Number(step)) && Number.isInteger(num)) &&
                (min == null || num >= min) &&
                (max == null || num <= max))
                return num;
            return undefined;
        },
        email: function({pattern}, val)
        {
            return this.text({pattern: pattern || emailRx}, val) 
        },
};

export function conform_val(val, {type, ...constraints})
{
    if(emptyVals.includes(val))
    {
        return constraints.required ? undefined : null;
    }
    if(type in validators)
    {
        return validators[type](constraints, val)
    }
    throw `Don't know how to conform type: ${type}`
}

function check_invariants(valid_inputs, invariants) 
{
    let errors = []
    if(valid_inputs && invariants)
    {
        Object.entries(invariants).forEach(([inv_key, invariant]) => 
            {
                if(invariant(valid_inputs) === false) 
                    errors.push(inv_key)
            })
    }
    return errors.length > 0 ? 
        {err: errors}:
        {val: valid_inputs}
}

export function construct_from(inputs, input_specs, invariants)
{
    let valid_inputs = {};
    let bad_inputs = [];
    for(const [input_key, spec] of Object.entries(input_specs))
    {
        const val = conform_val(inputs[input_key], spec);
        if(val === undefined)
            bad_inputs.push(input_key)
        else
            valid_inputs[input_key] = val;
    }
    if(bad_inputs.length > 0) 
        return {err: bad_inputs}
    return check_invariants(valid_inputs, invariants)
}

function inputView([id, props])
{
   return `<label ${props.required ? 'required' : ''}>
            ${props.label}
            <input id='${id}' name='${id}' ${Object.entries(props).map(([k,v]) => `${k}='${v}'`).join(' ')}>
        </label>` 
}

export default function userFormView(inputs)
{
    return `<form id='userForm'>
                ${Object.entries(inputs).map(kv => inputView(kv)).join('')}
                <input type='submit' id='userSubmit' value='Submit'>
            </form>`
}


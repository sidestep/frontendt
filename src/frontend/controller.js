import { inputs as user_inputs, invariants} from '../user_spec.js'
import { construct_from, conform_val } from '../input_validation.js'
import { htmlToDom, cmlCaseToWords } from '../utils.js'
import userFormView from './view.js'

//add display labels to specs
Object.entries(user_inputs).forEach(([input_key, spec]) => spec.label = cmlCaseToWords(input_key))

const _abcError = (id) => `Please enter ${cmlCaseToWords(id)} in alphabet characters`;
function errMsg(forField)
{
    switch(forField)
    {
        case lastName: return _abcError(forField.id);
        case firstName: return _abcError(forField.id);
        case age: return `Age must be between ${forField.min} and ${forField.max}`;
        case email: return 'Please enter a correct email';
        case custodian: return _abcError(forField.id);
        default: return `Invalid input: ${cmlCaseToWords(forField.id)}`;
    }
}

function showError(err_id)
{
    let msg = ""
    let badField = userForm.elements[err_id];
    if(badField != null)
        msg = errMsg(badField)
    else if(err_id == 'custodian_required')
    {
        badField = custodian;
        msg = "Please enter custodian for a minor";
    }
    badField.setCustomValidity(msg);
    badField.reportValidity();
}

function writeResult(result)
{
    var o = document.createElement("output");
    o.value = result;
    sumbitResults.prepend(o);
}
function onUserFormSubmit(e) {
    e.preventDefault();
    let user_data = Object.fromEntries(new FormData(userForm));
    if(clientValidate.checked)
    {
        console.log('validating on browser')
        const {err, val} = construct_from(user_data, user_inputs, invariants)
        if(err)
        {
            err.forEach(err_id => showError(err_id));
            return;
        }
        user_data = val;
    }
    fetch("https://mockend.com/AdamClements/coding-test/users",
    {
        method: "POST",
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user_data),
    }).then(async res => 
        {
            let resTxt = await res.text();
            if(!res.ok) 
                throw `HTTP ${res.status}: ${resTxt}`; 
            return resTxt
        })
    .then(resTxt => writeResult("OK: " + resTxt))
    .catch(err => writeResult(err))
}

let userForm = htmlToDom(userFormView(user_inputs));
document.getElementById('content').prepend(userForm);
userForm.setAttribute('novalidate', 'novalidate');
userForm.appendChild(htmlToDom(
`<label>
    Validate on browser
    <input type="checkbox" id="clientValidate" checked="checked" />
</label>`));
userForm.addEventListener('submit', onUserFormSubmit);

